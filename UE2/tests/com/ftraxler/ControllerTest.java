package com.ftraxler;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ControllerTest<T extends Controller>
{
    @Test
    void IsSingleton()
    {
        T instance1 = (T) Controller.getInstance();
        T instance2 = (T) Controller.getInstance();
        assertEquals(instance1.hashCode(), instance2.hashCode());
    }
}