package com.ftraxler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlightTest<T extends Flight>
{
    @Test
    void getPriceTest_100_1000()
    {
        T sut = (T) new Flight(null, new Airplane(null, null, 100), null, null, null, 1000);
        for (int i = 100; i > 70; i--)
        {
            assertEquals(1000, sut.getPrice());
            sut.decrementFreeSeats();
        }
        for (int i = 70; i > 30; i--)
        {
            assertEquals(700, sut.getPrice());
            sut.decrementFreeSeats();
        }
        for (int i = 30; i > 0; i--)
        {
            assertEquals(300, sut.getPrice());
            sut.decrementFreeSeats();
        }
    }
}