package com.ftraxler;

public class Flight
{
    private String _date;
    private String _estimatedDepartureTime;
    private String _estimatedArrivalTime;
    private int _numberOfFreeSeats;
    private int _maximumPrice;
    private Flightdescription _flightdescription;
    private Airplane _airplane;

    /**
     * @param flightdescription
     * @param airplane
     * @param date
     * @param estimatedDepartureTime
     * @param estimatedArrivalTime
     * @param maximumPrice
     */
    public Flight(Flightdescription flightdescription, Airplane airplane, String date, String estimatedDepartureTime, String estimatedArrivalTime, int maximumPrice)
    {

        _flightdescription = flightdescription;
        _airplane = airplane;
        _date = date;
        _estimatedDepartureTime = estimatedDepartureTime;
        _estimatedArrivalTime = estimatedArrivalTime;
        _maximumPrice = maximumPrice;
        _numberOfFreeSeats = airplane.getNumberOfSeats();
    }

    /**
     * Der Preis eines Fluges berechnet sich folgendermassen:
     * Die ersten 30% der Plaetze kosten 30% des Hoechstpreises, die naechsten 40% kosten
     * 70% des Hoechstpreises und die restlichen Plaetze 100% des Hoechstpreises.
     * <p>
     * z.B.: 100 Plaetze, Hoechstpreis >1000
     * Plaetze 1-30:     >300
     * Plaetze 31-70:   >700
     * Plaetze 71-100: >1000
     *
     * @return double
     */
    public double getPrice()
    {
        double seatPercentage = _numberOfFreeSeats / (double) _airplane.getNumberOfSeats();
        if (seatPercentage <= 0.3)
            return 0.3 * _maximumPrice;
        else if (seatPercentage > 0.3 && seatPercentage <= 0.7)
            return 0.7 * _maximumPrice;
        else
            return _maximumPrice;
    }

    public void incrementFreeSeats()
    {
        _numberOfFreeSeats++;
    }

    public void decrementFreeSeats()
    {
        _numberOfFreeSeats--;
    }

    public String getDate()
    {
        return _date;
    }

    public String getEstimatedDepartureTime()
    {
        return _estimatedDepartureTime;
    }

    public void setEstimatedDepartureTime(String newValue)
    {
        _estimatedDepartureTime = newValue;
    }

    public void setEstimatedArrivalTime(String newValue)
    {
        _estimatedArrivalTime = newValue;
    }

    public String getEstimatedArrivalTime()
    {
        return _estimatedArrivalTime;
    }

    public int getMaximumPrice()
    {
        return _maximumPrice;
    }

    public Flightdescription getFlightdescription()
    {
        return _flightdescription;
    }

    public Airplane getAirplane()
    {
        return _airplane;
    }
}