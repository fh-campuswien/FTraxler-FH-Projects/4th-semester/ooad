package com.ftraxler;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class Controller
{

    private ArrayList<Customer> _customers;
    private ArrayList<Flight> _flights;
    private ArrayList<Airplane> _airplanes;
    private ArrayList<Reservation> _reservations;
    private ArrayList<Flightdescription> _flightdescriptions;
    private static Controller _instance;

    /**
     * Privater Konstruktor. -> Singleton-Pattern
     */
    private Controller()
    {
        _customers = new ArrayList<>();
        _flights = new ArrayList<>();
        _airplanes = new ArrayList<>();
        _reservations = new ArrayList<>();
        _flightdescriptions = new ArrayList<>();
        _flightdescriptions.add(new Flightdescription("123", "vienna", "linz", "12:00", "12:30"));
        _flightdescriptions.add(new Flightdescription("321", "linz", "vienna", "18:00", "18:30"));
    }

    /**
     * Returniert immer die selbe Instanz der Klasse Controller. -> Singleton-Pattern
     *
     * @return Controller
     */
    public static synchronized Controller getInstance()
    {
        if (_instance == null)
            _instance = new Controller();
        return _instance;
    }

    /**
     * Gibt den Vector Fluege zurueck.
     *
     * @return java.util.Vector
     */
    public ArrayList<Flight> getFlights()
    {
        return _flights;
    }

    /**
     * Gibt den Vector Kunden zurueck.
     *
     * @return java.util.Vector
     */
    public ArrayList<Customer> getCustomers()
    {
        return _customers;
    }

    /**
     * @param c
     * @param f
     */
    public void bookFlight(Customer c, Flight f)
    {
        Double price = f.getPrice();
        f.decrementFreeSeats();

        _reservations.add(new Reservation(c, f, price));
    }

    /**
     * @param reservation
     */
    public void cancelReservation(Reservation reservation)
    {
        reservation.getFlight().incrementFreeSeats();
        _reservations.remove(reservation);
    }

    /**
     * Gibt den Vector Flugbeschreibungen zurueck.
     *
     * @return java.util.Vector
     */
    public ArrayList<Flightdescription> getFlightdescriptions()
    {
        return _flightdescriptions;
    }

    /**
     * Gibt den Vector Flugzeuge zurueck.
     *
     * @return java.util.Vector
     */
    public ArrayList<Airplane> getAirplanes()
    {
        return _airplanes;
    }

    /**
     * Gibt alle Buchungen eines bestimmten Kunden zurueck
     *
     * @param customer
     * @return java.util.Vector
     */
    public ArrayList<Reservation> getReservations(Customer customer)
    {
        ArrayList<Reservation> selection = new ArrayList<>();
        for (Reservation r : _reservations)
        {
            if (r.getCustomer() == customer)
                selection.add(r);
        }
        return selection;
    }

    /**
     * Gibt alle Kunden, die einen bestimmten Namen haben, zurueck.
     *
     * @param name
     * @return java.util.Vector
     */
    public ArrayList<Customer> getCustomers(String name)
    {
        return null;
    }

    /**
     * @param type
     * @param name
     * @param seats
     */
    public void insertAirplane(String type, String name, int seats)
    {
        _airplanes.add(new Airplane(type, name, seats));
    }

    /**
     * @param flightdescription
     * @param airplane
     * @param date
     * @param estimatedTimeOfDeparture
     * @param estimatedTimeOfArrival
     * @param priceLimit
     */
    public void insertFlight(Flightdescription flightdescription, Airplane airplane, String date, String estimatedTimeOfDeparture, String estimatedTimeOfArrival, int priceLimit)
    {
        _flights.add(new Flight(flightdescription, airplane, date, estimatedTimeOfDeparture, estimatedTimeOfArrival, priceLimit));
    }

    /**
     * @param name
     * @param zipCode
     * @param street
     * @param country
     */
    public void insertCustomer(String name, String zipCode, String street, String country)
    {
        _customers.add(new Customer(name, zipCode, street, country));
    }

    /**
     * Aendert die erwartete Abflugs- und Ankunftszeit eines bestimmten Fluges.
     *
     * @param f
     * @param estimatedDepartureTime
     * @param estimatedArrivalTime
     */
    public void changeFlightData(Flight f, String estimatedDepartureTime, String estimatedArrivalTime)
    {
        f.setEstimatedDepartureTime(estimatedDepartureTime);
        f.setEstimatedArrivalTime(estimatedArrivalTime);
    }

    /**
     * @param Flugnummer
     * @param Abflugsort
     * @param Ankunftsort
     * @param GeplanteAbflugszeit
     * @param GeplanteAnkunftszeit
     */
    public void insertFlightDescription(String flugnummer, String abflugsort, String ankunftsort, String geplanteAbflugszeit, String geplanteAnkunftszeit)
    {
        throw new NotImplementedException();
    }
}
