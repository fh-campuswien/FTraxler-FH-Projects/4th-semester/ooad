package com.ftraxler;

public class Customer
{
    private String _name;
    private String _street;
    private String _zipCode;
    private String _country;

    /**
     * @param name
     * @param zipCode
     * @param street
     * @param country
     */
    public Customer(String name, String zipCode, String street, String country)
    {

        _name = name;
        _zipCode = zipCode;
        _street = street;
        _country = country;
    }

    public String getName()
    {
        return _name;
    }

    public String getStreet()
    {
        return _street;
    }

    public String getZipCode()
    {
        return _zipCode;
    }

    public String getCountry()
    {
        return _country;
    }
}
