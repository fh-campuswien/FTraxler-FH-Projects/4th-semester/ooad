package com.ftraxler;

public class Airplane
{
    private String _type;
    private String _name;
    private int _numberOfSeats;

    public Airplane(String type, String name, int numberOfSeats)
    {
        _type = type;
        _name = name;
        _numberOfSeats = numberOfSeats;
    }

    public String getType()
    {
        return _type;
    }

    public String getName()
    {
        return _name;
    }

    public int getNumberOfSeats()
    {
        return _numberOfSeats;
    }

}
