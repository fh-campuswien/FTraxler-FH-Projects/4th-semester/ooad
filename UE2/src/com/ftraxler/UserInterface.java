package com.ftraxler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInterface
{
    private Controller _controller;

    public UserInterface()
    {
        _controller = Controller.getInstance();
    }

    public void changeFlightData()
    {
        writeLine("Select a flight");
        Flight flight = selectFlight();
        writeLine("Flight Data:");
        writeLine("ETD: " + flight.getEstimatedDepartureTime());
        writeLine("ETA: " + flight.getEstimatedArrivalTime());
        writeLine("Set new ETD:");
        String etd = readLine();
        writeLine("Set new ETA:");
        String eta = readLine();

        _controller.changeFlightData(flight, etd, eta);
        writeLine("Sucessfully changed data");
    }

    /**
     * Mit dieser Methode werden die Daten eines Flugzeuges vom User abgefragt und an
     * den Controller uebergeben.
     */
    public void insertAirplane()
    {
        writeLine("Airplane Type:");
        String type = readLine();
        writeLine("Airplane Name:");
        String name = readLine();
        writeLine("Seat Capacity:");
        Integer seats = readInteger();

        _controller.insertAirplane(type, name, seats);

        writeLine("Airplane inserted");
    }

    /**
     * Mit dieser Methode werden die Daten eines Fluges vom User abgefragt und an den
     * Controller uebergeben.
     */
    public void insertFlight()
    {
        writeLine("Select Flight Description:");
        Flightdescription description = selectFlightDescription();
        writeLine("Select a plane:");
        Airplane plane = selectAirplane();
        writeLine("Enter a Date:");
        String date = readLine();
        writeLine("Enter an estimated departure time:");
        String etd = readLine();
        writeLine("Enter an estimated arrival time:");
        String eta = readLine();
        writeLine("Enter tha maximum seat costs:");
        Integer limit = readInteger();

        _controller.insertFlight(description, plane, date, etd, eta, limit);

        writeLine("Flight inserted.");
    }

    /**
     * Mit dieser Methode werden die Daten eines neuen Kunden vom User abgefragt und
     * an den Controller uebergeben.
     */
    public void insertCustomer()
    {
        writeLine("Enter Name:");
        String name = readLine();
        writeLine("Enter ZIP Code:");
        String zip = readLine();
        writeLine("Enter Street and House Number:");
        String street = readLine();
        writeLine("Enter Country:");
        String county = readLine();

        _controller.insertCustomer(name, zip, street, county);

        writeLine("Customer inserted");
    }

    /**
     * Mit dieser Methode wird ein Flight fuer einen Kunden gebucht. Dabei koennen sowohl
     * der Flight als auch der Customer vom User ausgewaehlt werden.
     */
    public void bookFlight()
    {
        writeLine("Available Flights:");
        Flight flight = selectFlight();
        writeLine("Enter Customer Name:");
        String name = readLine();
        Customer customer = getCustomer(name);
        if (customer == null)
        {
            writeLine("Customer does not exist. Cancelling Booking. Please insert the customer first.");
            insertCustomer();
            return;
        }

        _controller.bookFlight(customer, flight);
        writeLine("Booking successful");
    }

    private Customer getCustomer(String name)
    {
        for (Customer c : _controller.getCustomers())
        {
            if (c.getName().equals(name))
                return c;
        }
        return null;
    }

    /**
     * Mit dieser Methode wird eine Integer-Zahl von stdin eingelesen, die von
     * lowerlimit bis upperlimit betragen darf.
     *
     * @return int
     */
    private int readInteger()
    {
        String input = readLine();
        try
        {
            return Integer.parseInt(input);
        } catch (Exception ex)
        {
            writeLine("Invalid input. please try again.");
            return readInteger();
        }
    }

    /**
     * Durch Aufruf dieser Methode kann vom User ein Flight ausgewaehlt werden. Alle
     * vorhandenen Fluege sind waehlbar.
     *
     * @return Flight
     */
    private Flight selectFlight()
    {
        for (Flight f : _controller.getFlights())
        {
            writeLine(f.getFlightdescription().getFlightNumber());
        }
        writeLine("Enter a Flight Number:");
        String flight = readLine();
        for (Flight f : _controller.getFlights())
        {
            if (f.getFlightdescription().getFlightNumber().equals(flight))
                return f;
        }
        writeLine("Error. Invalid Flight Number");
        throw new IllegalArgumentException();
    }

    /**
     * Mit dieser Methode kann eine Reservation storniert werden. Dazu wird zuerst der
     * Customer ausgewaehlt. Anschliessend werden alle Buchungen dieses Kunden angezeigt.
     * Der User waehlt dann die zu stornierende Reservation.
     */
    public void cancelReservation()
    {
        writeLine("Cancel Reservation for Customer:");
        String name = readLine();
        Customer customer = getCustomer(name);
        if (customer == null)
        {
            writeLine("Customer not in our System.");
            return;
        }
        writeLine("Select reservation:");
        Reservation reservation = selectReservation(customer);

        _controller.cancelReservation(reservation);
        writeLine("Reservation cancelled");
    }

    /**
     * Mit dieser Methode wird ein String von stdin gelesen.
     *
     * @return java.lang.String
     */
    private String readLine()
    {
        String value = "\0";
        BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
        try
        {
            value = inReader.readLine();
        } catch (IOException e)
        {
        }
        return value.trim();
    }

    /**
     * Durch Aufruf dieser Methode werden alle Flugzeuge angezeigt, von denen der User
     * dann eines waehlen kann.
     *
     * @return Airplane
     */
    private Airplane selectAirplane()
    {
        for (Airplane a : _controller.getAirplanes())
        {
            writeLine(a.getName());
        }
        writeLine("Enter an airplane name:");
        String name = readLine();
        for (Airplane a : _controller.getAirplanes())
        {
            if (a.getName().equals(name))
                return a;
        }
        writeLine("Error. Invalid Name");
        throw new IllegalArgumentException();
    }

    /**
     * Durch Aufruf dieser Methode werden zuerst alle Flugbeschreibungen aufgelistet.
     * Anschliessend kann der User eine dieser Flugbeschreibungen waehlen.
     *
     * @return Flightdescription
     */
    private Flightdescription selectFlightDescription()
    {
        writeLine("Available flight descriptions:");
        for (Flightdescription f : _controller.getFlightdescriptions())
        {
            writeLine(f.getFlightNumber());
        }
        writeLine("Enter a flight number:");
        String number = readLine();
        for (Flightdescription f : _controller.getFlightdescriptions())
        {
            if (f.getFlightNumber().equals(number))
                return f;
        }
        writeLine("Error. Invalid Number");
        throw new IllegalArgumentException();
    }

    /**
     * Durch Aufruf dieser Methode werden zuerst alle Buchungen eines Kunden
     * aufgelistet. Anschliessend kann der User eine dieser Buchungen auswaehlen.
     *
     * @param customer
     * @return Reservation
     */
    private Reservation selectReservation(Customer customer)
    {
        for (Reservation r : _controller.getReservations(customer))
        {
            writeLine(r.getFlight().getFlightdescription().getFlightNumber());
        }
        String selection = readLine();
        for (Reservation r : _controller.getReservations(customer))
        {
            if (r.getFlight().getFlightdescription().getFlightNumber().equals(selection))
                return r;
        }
        writeLine("Error. Invalid Reservation number");
        throw new IllegalArgumentException();
    }

    public void start()
    {
        Menu menu = new Menu();
        menu.setTitel("FH Airport Tower");
        menu.insert('a', "Add Airplane");
        menu.insert('b', "Add Flight");
        menu.insert('c', "Add Customer");
        menu.insert('d', "Book Flight");
        menu.insert('e', "Change Flight");
        menu.insert('f', "Cancel Reservationqsa");
        menu.insert('q', "Quit");
        char choice;
        while ((choice = menu.exec()) != 'q')
        {
            try
            {
                switch (choice)
                {
                    case 'a':
                        insertAirplane();
                        break;
                    case 'b':
                        insertFlight();
                        break;
                    case 'c':
                        insertCustomer();
                        break;
                    case 'd':
                        bookFlight();
                        break;
                    case 'e':
                        changeFlightData();
                        break;
                    case 'f':
                        cancelReservation();
                        break;
                }
            } catch (Exception e)
            {
            }
        }
        System.out.println("Program finished");
    }

    private void writeLine(String text)
    {
        System.out.println(text);
    }

}
