package com.ftraxler;

public class Reservation
{
    private double _price;
    private Flight _flight;
    private Customer _customer;

    /**
     * @param customer
     * @param flight
     * @param price
     */
    public Reservation(Customer customer, Flight flight, double price)
    {

        _customer = customer;
        _flight = flight;
        _price = price;
    }

    /**
     * @return Flight
     */
    public Flight getFlight()
    {
        return _flight;

    }

    /**
     * @return Customer
     */
    public Customer getCustomer()
    {
        return _customer;

    }

    /**
     * @return double
     */
    public double getPrice()
    {
        return _price;
    }
}
