package com.ftraxler;

public class Flightdescription
{
    private String _flightNumber;
    private String _origination;
    private String _destination;
    private String _plannedDepartureTime;
    private String _plannedArrivalTime;

    /**
     * @param flightNumber
     * @param origination
     * @param destination
     * @param plannedDepartureTime
     * @param plannedArrivalTime
     */
    public Flightdescription(String flightNumber, String origination, String destination, String plannedDepartureTime, String plannedArrivalTime)
    {

        _flightNumber = flightNumber;
        _origination = origination;
        _destination = destination;
        _plannedDepartureTime = plannedDepartureTime;
        _plannedArrivalTime = plannedArrivalTime;
    }

    public String getFlightNumber()
    {
        return _flightNumber;
    }

    public String getOrigination()
    {
        return _origination;
    }

    public String getDestination()
    {
        return _destination;
    }

    public String getPlannedDepartureTime()
    {
        return _plannedDepartureTime;
    }

    public String getPlannedArrivalTime()
    {
        return _plannedArrivalTime;
    }
}
