package com.ftraxler;

import java.util.List;

public class Person extends BasePerson
{
    private String _skypeAddress;
    private List<String> _hobbies;

    public String getSkypeAddress()
    {
        return _skypeAddress;
    }

    public void setSkypeAddress(String skypeAddress)
    {
        _skypeAddress = skypeAddress;
    }

    public List<String> getHobbies()
    {
        return _hobbies;
    }

    public void setHobbies(List<String> hobbies)
    {
        _hobbies = hobbies;
    }
}
