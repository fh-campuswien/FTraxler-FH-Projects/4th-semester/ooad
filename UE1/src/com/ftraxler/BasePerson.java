package com.ftraxler;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.List;

public class BasePerson
{
    private String _name;
    private String _lastName;
    private LocalDate _dateOfBirth;
    private String _mailAddress;
    private List<Address> _addresses;

    public BasePerson()
    {
        _addresses = new ArrayList<>();
    }

    public String getName()
    {
        return _name;
    }

    public void setName(String name)
    {
        _name = name;
    }

    public String getLastName()
    {
        return _lastName;
    }

    public void setLastName(String lastName)
    {
        _lastName = lastName;
    }

    public LocalDate getDateOfBirth()
    {
        return _dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth)
    {
        _dateOfBirth = dateOfBirth;
    }

    public String getMailAddress()
    {
        return _mailAddress;
    }

    public void setMailAddress(String mailAddress)
    {
        String regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        if (mailAddress.matches(regex))
        {
            _mailAddress = mailAddress;
        } else
            throw new IllegalArgumentException("E-Mail Address is in the wrong format");
    }

    public List<Address> getAddresses()
    {
        return _addresses;
    }

    public void addAddress(Address addresse)
    {
        _addresses.add(addresse);
    }

    public boolean isOlderThan(Person toCompare)
    {
        return _dateOfBirth.isBefore(toCompare.getDateOfBirth());
    }

    public boolean hasBirthday()
    {
        LocalDate today = LocalDate.now();
        return _dateOfBirth.getMonth() == today.getMonth() && _dateOfBirth.getDayOfMonth() == today.getDayOfMonth();
    }
}
