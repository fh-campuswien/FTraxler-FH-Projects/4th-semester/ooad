package com.ftraxler;

import java.util.ArrayList;
import java.util.List;

public class AddressBook
{
    private List<Person> _entries;

    public AddressBook()
    {
        _entries = new ArrayList<>();
    }

    public List<Person> getEntries()
    {
        return _entries;
    }

    public void add(Person person)
    {
        _entries.add(person);
    }

    public List<Person> searchForName(String name)
    {
        List<Person> people = new ArrayList<>();
        for (Person p : _entries)
        {
            if (p.getName() == name)
                people.add(p);
        }
        return people;
    }

    public List<Person> getBirthdayPeople()
    {
        List<Person> people = new ArrayList<>();
        for (Person p : _entries)
        {
            if (p.hasBirthday())
                people.add(p);
        }
        return people;
    }
}
