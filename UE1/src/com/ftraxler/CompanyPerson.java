package com.ftraxler;

public class CompanyPerson extends BasePerson
{
    private String _companyName;

    public String getCompanyName()
    {
        return _companyName;
    }

    public void setCompanyName(String companyName)
    {
        _companyName = companyName;
    }
}
