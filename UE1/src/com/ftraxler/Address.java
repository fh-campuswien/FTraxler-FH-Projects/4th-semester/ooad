package com.ftraxler;

import com.sun.javaws.exceptions.InvalidArgumentException;

public class Address
{
    private String _streetName;
    private int _houseNumber;
    private int _zipCode;
    private String _city;

    public String getStreetName()
    {
        return _streetName;
    }

    public void setStreetName(String streetName)
    {
        _streetName = streetName;
    }

    public int getHouseNumber()
    {
        return _houseNumber;
    }

    public void setHouseNumber(int houseNumber)
    {
        _houseNumber = houseNumber;
    }

    public int getZipCode()
    {
        return _zipCode;
    }

    public void setZipCode(int zipCode)
    {
        _zipCode = zipCode;
    }

    public String getCity()
    {
        return _city;
    }

    public void setCity(String city)
    {
        _city = city;
    }

    public static String getDistrictFromAddress(Address address)
    {
        String zipCode = Integer.toString(address.getZipCode());
        switch (zipCode.charAt(0))
        {
            case '1':
                return "Wien";
            case '2':
            case '3':
                return "Niederösterreich";
            case '4':
                return "Oberösterreich";
            case '5':
                return "Salzburg";
            case '6':
                return "Nordtirol und Vorarlberg";
            case '7':
                return "Burgenland";
            case '8':
                return "Steiermark";
            case '9':
                return "Kärnten und Osttirol";
            default:
                throw new IllegalArgumentException("ZIP code is in an invalid format");
        }
    }
}
