package com.ftraxler;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class BasePersonTest<T extends  Person>
{


    @Test
    void isOlderThan()
    {
        // arrange
        T florian = (T) new Person();
        florian.setName("Florian");
        florian.setDateOfBirth(LocalDate.of(1993,12,07));
        T bernhard = (T) new Person();
        bernhard.setName("Bernhard");
        bernhard.setDateOfBirth(LocalDate.of(1996,10,19));
        // act and assert
        assertTrue(florian.isOlderThan(bernhard));
        assertFalse(bernhard.isOlderThan(florian));
    }

    @Test
    void setsValidMailAddress()
    {
        String mailAddress = "florian.traxler@stud.fh-campuswien.ac.at";
        T p = (T) new Person();
        assertDoesNotThrow(() -> p.setMailAddress(mailAddress));
    }

    @Test
    void throwsOnInvalidMailAddress()
    {
        String mailAddress = "florian.traxler[at]stud.fh-campuswien.ac.at";
        T p = (T) new Person();
        assertThrows(IllegalArgumentException.class, () -> p.setMailAddress(mailAddress));
    }
}