package com.ftraxler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AddressBookTest
{
    private AddressBook _systemUnderTest;

    @BeforeEach
    void setUp()
    {
        _systemUnderTest = new AddressBook();
    }

    @Test
    void initializesList()
    {
        assertNotNull(_systemUnderTest.getEntries());
    }


    @Test
    void addsAndGetsEntries()
    {
        Person testPerson = new Person();
        _systemUnderTest.add(testPerson);
        assertTrue(_systemUnderTest.getEntries().contains(testPerson));
    }

    @Test
    void searchForName()
    {
        // arrange
        Person testPerson1 = new Person();
        testPerson1.setName("Florian");
        _systemUnderTest.add(testPerson1);
        Person testPerson2 = new Person();
        testPerson2.setName("Nobody");
        _systemUnderTest.add(testPerson2);
        // act
        List<Person> results = _systemUnderTest.searchForName("Florian");
        // assert
        assertTrue(results.get(0).getName() == "Florian");
    }

    @Test
    void getBirthdayPeople()
    {
        // arrange
        Person testPerson1 = new Person();
        testPerson1.setName("Florian");
        testPerson1.setDateOfBirth(LocalDate.of(1992,2,29));
        _systemUnderTest.add(testPerson1);
        Person testPerson2 = new Person();
        testPerson2.setName("Nobody");
        testPerson2.setDateOfBirth(LocalDate.now());
        _systemUnderTest.add(testPerson2);
        // act
        List<Person> results = _systemUnderTest.getBirthdayPeople();
        // assert
        assertTrue(results.get(0).getName() == "Nobody");
    }
}